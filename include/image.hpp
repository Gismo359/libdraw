#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <filesystem>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <vector>

namespace draw {
  template <typename Type>
  int sign(Type const value) {
    return (value >= Type{ 0 }) - (value <= Type{ 0 });
  }

  inline uint8_t blend_one_minus(uint8_t const fore, uint8_t const back, float const alpha) {
    return static_cast<uint8_t>(std::round(fore * alpha + back * (1 - alpha)));
  }

  struct color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    float a;

    color(uint8_t const r, uint8_t const g, uint8_t const b, float const a = 1.0f) : r{ r }, g{ g }, b{ b }, a{ a } {}
    color(uint8_t const l, float const a = 1.0f) : r{ l }, g{ l }, b{ l }, a{ a } {}
    color() = default;
  };

  struct point {
    double x;
    double y;
    point operator*(double const other) const {
      return { x * other, y * other };
    }
  };

  /**
   * \brief Determines whether a point is inside, outside or on the border of a polygon
   * \param pt Point that is being checked
   * \param points List of points that define the polygon
   * \return -1 if the points lies outside the polygon, 0 if it is on the border and 1 if it lies inside the polygon
   */
  inline int point_in_poly(point const pt, std::vector<point> points) {
    auto wn = 0;

    for (auto point_id = 0u; point_id < points.size(); point_id++) { // edge from V[i] to  V[i+1]
      auto const pt1 = points.at(point_id);
      point pt2;
      if (point_id + 1 >= points.size()) {
        pt2 = points.front();
      } else {
        pt2 = points.at(point_id + 1);
      }

      auto const rel_pos = sign((pt2.x - pt1.x) * (pt.y - pt1.y) - (pt.x - pt1.x) * (pt2.y - pt1.y));

      if (rel_pos != 0) {
        auto const conditions = { pt1.y <= pt.y, pt2.y > pt.y, rel_pos > 0 };
        if (std::min(conditions)) {
          wn += 1;
        } else if (!std::max(conditions)) {
          wn -= 1;
        }
      }
    }
    return wn != 0;
  }

  struct image {
  public:
    enum blend_mode
    {
      Over,
      Max,
      Min,
      OneMinus
    };

   private:
    uint32_t width_;
    uint32_t height_;

    std::vector<color> data_;

    blend_mode blending_mode_ = OneMinus;

  public:
    blend_mode blending_mode() const {
      return blending_mode_;
    }

    void set_blending_mode(blend_mode const new_mode) {
        blending_mode_ = new_mode;
    }

    virtual ~image() = default;

    /**
     * \brief Constructs an image with given dimensions
     * \param width Width of the new image
     * \param height Height of the new image
     */
    image(uint32_t const width, uint32_t const height) : width_{ width }, height_{ height } {
      data_.resize(width * height);
    }

    /**
     * \brief Constructs an image with given dimensions, and fills it with a color
     * \param width Width of the new image
     * \param height Height of the new image
     * \param c Color with which to fill
     */
    image(uint32_t const width, uint32_t const height, color const c) : image{ width, height } {
      image::clear(c);
    }

    /**
     * \brief Gives the height of the image
     * \return Number of pixels vertically
     */
    uint32_t height() const {
      return height_;
    }

    /**
     * \brief Gives the width of the image
     * \return Number of pixels horizontally
     */
    uint32_t width() const {
      return width_;
    }

    /**
     * \brief Gives a view to the pixel data in this image
     * \return Readonly view of this image
     */
    std::vector<color> const &data() const {
      return data_;
    }

    /**
     * \brief Returns the color of a pixel at a position
     * \param x Horizontal position of the pixel
     * \param y Vertical position of the pixel
     * \return Current color of the pixel
     */
    virtual color at(double const x, double const y) const {
      return data_[static_cast<uint32_t>(std::round(y) * width() + std::round(x))];
    }

    /**
     * \brief Returns the color of a pixel at a position
     * \param pt Position of the pixel
     * \return Current color of the pixel
     */
    virtual color at(point const pt) const {
      return at(pt.x, pt.y);
    }

    /**
     * \brief Saves the image to the file system as a TARGA file
     * \param filename Path to the output image
     */
    void save_tga(std::filesystem::path filename) const {
      if (width() > std::numeric_limits<uint16_t>::max() || height() > std::numeric_limits<uint16_t>::max()) {
        throw std::runtime_error{ "Image is too big for TGA image" };
      }

      auto ext = filename.extension().string();
      std::transform(std::begin(ext), std::end(ext), std::begin(ext), [](char const ch) {
        if (ch >= 'a' && ch <= 'z') {
          return ch;
        } else if (ch >= 'A' && ch <= 'Z') {
          return static_cast<char>(ch - 'A' + 'a');
        } else {
          return ch;
        }
      });

      if (ext != ".tga") {
        filename += ".tga";
      }

      std::ofstream file{ filename };

      // Unsupported/irrelevant
      file.put(0);
      file.put(0);

      // TGA is uncompressed RGB
      file.put(2);

      // Unsupported/irrelevant
      file.put(0);
      file.put(0);
      file.put(0);
      file.put(0);
      file.put(0);
      file.put(0);
      file.put(0);
      file.put(0);
      file.put(0);

      // Width and height are little endian
      file.put(static_cast<char>((width() & 0x00FF)));
      file.put(static_cast<char>((width() & 0xFF00) >> 8));
      file.put(static_cast<char>((height() & 0x00FF)));
      file.put(static_cast<char>((height() & 0xFF00) >> 8));

      // Bit count for each pixel - 8 bits per channel (RGB)
      file.put(32);

      file.put(0);

      for (auto &&color : data()) {
        file.put(static_cast<char>(color.b));
        file.put(static_cast<char>(color.g));
        file.put(static_cast<char>(color.r));
        file.put(static_cast<char>(std::round(color.a * 255)));
      }
    }

    /**
     * \brief Clears the image and fills it with a color
     * \param c The color used to fill the image
     */
    virtual void clear(color const c) {
      std::fill(std::begin(data_), std::end(data_), c);
    }

    /**
     * \brief Sets the color of a pixel
     * \param x Horizontal position of the pixel
     * \param y Vertical position of the pixel
     * \param new_color Color of the pixel
     */
    virtual void set_color(double const x, double const y, color const new_color) {
      if (x >= 0 && x < width() && y >= 0 && y < height()) {
        auto const color_idx = static_cast<uint32_t>(std::round(y) * width() + std::round(x));
        switch (blending_mode())
        {
        case Over: 
          data_[color_idx] = new_color;
        case Max:
          data_[color_idx] = {
            std::min(data_[color_idx].r, new_color.r),
            std::min(data_[color_idx].g, new_color.g),
            std::min(data_[color_idx].b, new_color.b)
          };
          break;
        case Min:
          data_[color_idx] = {
            std::max(data_[color_idx].r, new_color.r),
            std::max(data_[color_idx].g, new_color.g),
            std::max(data_[color_idx].b, new_color.b)
          };
          break;
        case OneMinus: 
          data_[color_idx] = {
            blend_one_minus(new_color.r, data_[color_idx].r, new_color.a),
            blend_one_minus(new_color.g, data_[color_idx].g, new_color.a),
            blend_one_minus(new_color.b, data_[color_idx].b, new_color.a)
          };
          break;
        default: ;
        }
      }
    }

    /**
     * \brief Sets the color of a pixel
     * \param pt Position of the pixel
     * \param c Color of the pixel
     */
    virtual void set_color(point const pt, color const c) {
      set_color(pt.x, pt.y, c);
    }

    /**
     * \brief Draws a line between two points
     * \param x1 X coordinate of the beginning of the line
     * \param y1 Y coordinate of the beginning of the line
     * \param x2 X coordinate of the ending of the line
     * \param y2 Y coordinate of the ending of the line
     * \param c Color of the line
     */
    virtual void draw_line(double const x1, double const y1, double const x2, double const y2, color const c) {
      auto const dx   = std::round(x1) - std::round(x2);
      auto const dy   = std::round(y1) - std::round(y2);
      auto const dmax = std::max(std::abs(dx), std::abs(dy));
      auto const ux   = dx / dmax;
      auto const uy   = dy / dmax;

      for (auto idx = 0; idx < dmax; idx++) {
        set_color(x1 - idx * ux, y1 - idx * uy, c);
      }
    }

    /**
     * \brief Draws a line between two points
     * \param pt1 Beginning point of the line
     * \param pt2 Ending point of the line
     * \param c Color of the line
     */
    void draw_line(point const pt1, point const pt2, color const c) {
      draw_line(pt1.x, pt1.y, pt2.x, pt2.y, c);
    }

    /**
     * \brief Draws a polygon defined by a list of points
     * \param points The points that define the polygon
     * \param c The color of the polygon
     */
    virtual void draw_poly(std::vector<point> const &points, color c) {
      for (auto point_id = 0u; point_id < points.size() - 1; point_id++) {
        auto const pt1 = points.at(point_id);
        auto const pt2 = points.at(point_id + 1);
        draw_line(pt1, pt2, c);
      }
      draw_line(points.back(), points.front(), c);
    }

    /**
     * \brief Fills a polygon defined by a list of points
     * \param points The points that define the polygon
     * \param fill_color The color of the inside of the polygon
     */
    virtual void fill_poly(std::vector<point> const &points, color const fill_color) {
      double xmin = width();
      double xmax = 0;
      double ymin = height();
      double ymax = 0;

      for (auto pt : points) {
        if (pt.x < xmin) {
          xmin = pt.x;
        } else if (pt.x > xmax) {
          xmax = pt.x;
        }

        if (pt.y < ymin) {
          ymin = pt.y;
        } else if (pt.y > ymax) {
          ymax = pt.y;
        }
      }

      for (auto y = ymin; y <= ymax; y += 1.0) {
        for (auto x = xmin; x <= xmax; x += 1) {
          if (point_in_poly({ x, y }, points)) {
            set_color(x, y, fill_color);
          }
        }
      }
    }

    /**
     * \brief Fills a polygon defined by a list of points
     * \param points The points that define the polygon
     * \param fill_color The color of the inside of the polygon
     * \param border_color The color of the border of the polygon
     */
    virtual void fill_poly(std::vector<point> const &points, color fill_color, color border_color) {
      fill_poly(points, fill_color);
      draw_poly(points, border_color);
    }

    /**
     * \brief Draws a rectangle defined by two points
     * \param x1 X coordinate of the rectangle's bottom left corner
     * \param y1 Y coordinate of the rectangle's bottom left corner
     * \param x2 X coordinate of the rectangle's upper right corner
     * \param y2 Y coordinate of the rectangle's upper right corner
     * \param c Color of the rectangle
     */
    virtual void draw_rect(double const x1, double const y1, double const x2, double const y2, color const c) {
      draw_poly({ { x1, y1 }, { x1, y2 }, { x2, y2 }, { x2, y1 } }, c);
    }

    /**
     * \brief Draws a rectangle defined by two points
     * \param pt1 Bottom left corner of the rectangle
     * \param pt2 Upper right corner of the rectangle
     * \param c Color of the rectangle
     */
    virtual void draw_rect(point pt1, point pt2, color c) {
      draw_rect(pt1.x, pt1.y, pt2.x, pt2.y, c);
    }

    /**
     * \brief Fills a rectangle defined by two points
     * \param x1 X coordinate of the rectangle's bottom left corner
     * \param y1 Y coordinate of the rectangle's bottom left corner
     * \param x2 X coordinate of the rectangle's upper right corner
     * \param y2 Y coordinate of the rectangle's upper right corner
     * \param fill_color The color of the inside of the rectangle
     */
    virtual void fill_rect(double const x1, double const y1, double const x2, double const y2, color const fill_color) {
      fill_poly({ { x1, y1 }, { x1, y2 }, { x2, y2 }, { x2, y1 } }, fill_color);
    }

    /**
     * \brief Fills a rectangle defined by two points
     * \param x1 X coordinate of the rectangle's bottom left corner
     * \param y1 Y coordinate of the rectangle's bottom left corner
     * \param x2 X coordinate of the rectangle's upper right corner
     * \param y2 Y coordinate of the rectangle's upper right corner
     * \param fill_color The color of the inside of the rectangle
     * \param border_color The color of the border of the rectangle
     */
    virtual void fill_rect(double const x1, double const y1, double const x2, double const y2, color const fill_color,
                           color const border_color) {
      fill_poly({ { x1, y1 }, { x1, y2 }, { x2, y2 }, { x2, y1 } }, fill_color, border_color);
    }

    /**
     * \brief Fills a rectangle defined by two points
     * \param pt1 Bottom left corner of the rectangle
     * \param pt2 Upper right corner of the rectangle
     * \param fill_color The color of the inside of the rectangle
     */
    virtual void fill_rect(point const pt1, point const pt2, color const fill_color) {
      fill_rect(pt1.x, pt1.y, pt2.x, pt2.y, fill_color);
    }

    /**
     * \brief Fills a rectangle defined by two points
     * \param pt1 Bottom left corner of the rectangle
     * \param pt2 Upper right corner of the rectangle
     * \param fill_color The color of the inside of the rectangle
     * \param border_color The color of the border of the rectangle
     */
    virtual void fill_rect(point const pt1, point const pt2, color const fill_color, color const border_color) {
      fill_rect(pt1.x, pt1.y, pt2.x, pt2.y, fill_color, border_color);
    }

    /**
     * \brief Draws a circle
     * \param x X coordinate of the circle's center
     * \param y Y coordinate of the circle's center
     * \param r Radius of the circle
     * \param c Color of the circle
     */
    virtual void draw_circle(double const x, double const y, double const r, color const c) {
      for (auto dx = 1; dx < std::round(r / std::sqrt(2.0)); dx++) {
        auto const dy = std::sqrt(r * r - dx * dx);

        // The circle is divided into 8 symmetrical sections (defined by the axes and the diagonals between them)
        set_color(x + dx, y + dy, c);
        set_color(x - dx, y + dy, c);
        set_color(x - dx, y - dy, c);
        set_color(x + dx, y - dy, c);
        set_color(x + dy, y + dx, c);
        set_color(x - dy, y + dx, c);
        set_color(x - dy, y - dx, c);
        set_color(x + dy, y - dx, c);
      }

      auto const dmax = std::round(r / std::sqrt(2.0));

      set_color(x + dmax, y + dmax, c);
      set_color(x + dmax, y - dmax, c);
      set_color(x - dmax, y + dmax, c);
      set_color(x - dmax, y - dmax, c);

      set_color(x + r, y, c);
      set_color(x - r, y, c);
      set_color(x, y + r, c);
      set_color(x, y - r, c);
    }

    /**
     * \brief Draws a circle
     * \param pt Center of the circle
     * \param r Radius of the circle
     * \param c Color of the circle
     */
    virtual void draw_circle(point const pt, int16_t const r, color const c) {
      draw_circle(pt.x, pt.y, r, c);
    }

    /**
     * \brief Draws a circle
     * \param x X coordinate of the circle's center
     * \param y Y coordinate of the circle's center
     * \param r Radius of the circle
     * \param c The color of the inside of the rectangle
     */
    virtual void fill_circle(double const x, double const y, double const r, color const c) {
      for (auto dx = 1; dx < std::floor(r / std::sqrt(2.0)); dx++) {
        auto const dy = std::sqrt(r * r - dx * dx);

        for (auto dy2 = 0; dy2 < std::round(dy - dx); dy2++) {
          // The circle is divided into 8 symmetrical sections (defined by the axes and the diagonals between them)
          set_color(x + dx, y + (dy - dy2), c);
          set_color(x - dx, y + (dy - dy2), c);
          set_color(x - dx, y - (dy - dy2), c);
          set_color(x + dx, y - (dy - dy2), c);
          set_color(x + (dy - dy2), y + dx, c);
          set_color(x - (dy - dy2), y + dx, c);
          set_color(x - (dy - dy2), y - dx, c);
          set_color(x + (dy - dy2), y - dx, c);
        }

        auto const dmax = std::round(std::round(dy - dx));

        set_color(x + dx, y + (dy - dmax), c);
        set_color(x - dx, y + (dy - dmax), c);
        set_color(x - dx, y - (dy - dmax), c);
        set_color(x + dx, y - (dy - dmax), c);
      }

      for (auto d = 0; d < r; d++) {
        set_color(x + d, y, c);
        set_color(x - d, y, c);
        set_color(x, y + d, c);
        set_color(x, y - d, c);
      }
    }

    /**
     * \brief Draws a circle
     * \param x X coordinate of the circle's center
     * \param y Y coordinate of the circle's center
     * \param r Radius of the circle
     * \param fill_color The color of the inside of the rectangle
     * \param border_color The color of the border of the rectangle
     */
    virtual void fill_circle(double const x, double const y, double const r, color const fill_color, color const border_color) {
      fill_circle(x, y, r, fill_color);
      draw_circle(x, y, r, border_color);
    }

    /**
     * \brief Draws a circle
     * \param pt Center of the circle
     * \param r Radius of the circle
     * \param fill_color The color of the inside of the rectangle
     */
    virtual void fill_circle(point const pt, double const r, color const fill_color) {
      fill_circle(pt.x, pt.y, r, fill_color);
    }

    /**
     * \brief Draws a circle
     * \param pt Center of the circle
     * \param r Radius of the circle
     * \param fill_color The color of the inside of the rectangle
     * \param border_color The color of the border of the rectangle
     */
    virtual void fill_circle(point const pt, double const r, color const fill_color, color const border_color) {
      fill_circle(pt.x, pt.y, r, fill_color, border_color);
    }

    /**
     * \brief Draws a curve by interpolating between points
     * \param points A vector of points that are used to guide the curve
     * \param c The color of the curve
     * \param step_count Number of lines per interpolation (lower = faster but blocky)
     * \param tension The 'weight/inertia' of the curve. Controls how much adjacent points affect the curve
     */
    virtual void draw_spline(std::vector<point> const &points, color const c, uint16_t const step_count = 100,
                             double const tension = 0.5) {
      std::vector<point> tangents;

      tangents.push_back({ 0, 0 });
      for (auto point_id = 0u; point_id < points.size() - 2; point_id++) {
        auto const pt1 = points.at(point_id);
        auto const pt2 = points.at(point_id + 2);
        auto const x   = (pt2.x - pt1.x) * tension;
        auto const y   = (pt2.y - pt1.y) * tension;
        tangents.push_back({ x, y });
      }
      tangents.push_back({ 0, 0 });

      auto last_pt = points.front();

      auto const step_size = 1.0 / step_count;
      for (auto dx = 0.0; dx < points.size() - 1; dx += step_size) {
        auto const curve_id = static_cast<uint16_t>(std::floor(dx));

        auto const t  = dx - curve_id;
        auto const t2 = std::pow(t, 2.0);
        auto const t3 = std::pow(t, 3.0);

        auto const hp1 = 2 * t3 - 3 * t2 + 1;
        auto const hp2 = -2 * t3 + 3 * t2;
        auto const ht1 = t3 - 2 * t2 + t;
        auto const ht2 = t3 - t2;

        auto const pt1  = points.at(curve_id);
        auto const pt2  = points.at(curve_id + 1);
        auto const tan1 = tangents.at(curve_id);
        auto const tan2 = tangents.at(curve_id + 1);

        auto const x = pt1.x * hp1 + pt2.x * hp2 + tan1.x * ht1 + tan2.x * ht2;
        auto const y = pt1.y * hp1 + pt2.y * hp2 + tan1.y * ht1 + tan2.y * ht2;

        point pt{ x, y };

        draw_line(last_pt, pt, c);

        last_pt = pt;
      }
      draw_line(last_pt, points.back(), c);
    }
  };
} // namespace draw

#endif // IMAGE_HPP

